﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

using namespace std;

int main() 
{
    const int N = 5;
    
    int array[N][N];

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            array[i][j] = i + j;
        }
    }

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            cout << array[i][j] << " ";
        }
        cout << endl;
    }

    int sum = 0;
    time_t t;
    time(&t);
    int day = localtime(&t)->tm_mday;

    for (int x = 0; x < N; x++)
    {
        sum += array[day % N][x];
    }
    cout << sum << endl;

    return 0;
}

